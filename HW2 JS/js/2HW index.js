// Завдання
// HW2 - alert-prompt-confirm

// Теоретичні питання:
// Які існують типи даних у Javascript?
// Типи данних є прості , та об'єктні
// string, number, Boolean, null , undefined, function, object, Array, BigInt , infinity 


// У чому різниця між == і ===?
// Не суворе дорівнює порівнює без перевірки типу данних , суворе дорівнює порівнює з 
// приведенням до типів.  

// Що таке оператор? Оператор це символ або фраза , який вказує JS , що робити зі змінними 
// та значеннями.


// Завдання
// Реалізувати просту програму на Javascript, яка взаємодіятиме з користувачем за допомогою 
// модальних вікон браузера - alert, prompt, confirm. Завдання має бути виконане на чистому 
// Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Отримати за допомогою модального вікна браузера дані користувача: ім'я та вік.
// Якщо вік менше 18 років - показати на екрані повідомлення: You are not allowed to visit this 
// website.
// Якщо вік від 18 до 22 років (включно) – показати вікно з наступним повідомленням:
//  Are you sure you want to continue? і кнопками Ok, Cancel. Якщо користувач натиснув Ok, 
//  показати на екрані повідомлення: Welcome, + ім'я користувача. Якщо користувач натиснув 
// Cancel, 
//  показати на екрані повідомлення: You are not allowed to visit this website.
// Якщо вік більше 22 років – показати на екрані повідомлення: Welcome, + ім'я користувача.
// Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.
// Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів ім'я, або 
// при введенні віку вказав не число - запитати ім'я та вік наново (при цьому дефолтним 
// значенням для кожної зі змінних має бути введена раніше інформація).




let userName;
let userAge;

do {
    userName = prompt('Enter your name');
    if (userName === null || userName.trim() === '' || !isNaN(userName)) {
        alert('Incorrect name entry! Enter a name that does not contain numbers');
    }
} while (userName === null || userName.trim() === '' || !isNaN(userName));

do {
    userAge = prompt('Enter your age');
    if (userAge === null || userAge.trim() === '' || isNaN(userAge)) {
        alert('Incorrect age input! Please enter a number according to your age.');
    }
} while (userAge === null || userAge.trim() === '' || isNaN(userAge));

if (userAge < 18) {
    alert('You are not allowed to visit this website.');
} else if (userAge > 22) {
    alert(`Welcome, ${userName}`);
} else {
    let result = confirm('Are you sure you want to continue?');
    if (result) {
        alert(`Welcome, ${userName}`);
    } else {
        alert('You are not allowed to visit this website');
    }
}











