// Завдання
// HW3 - find-multiples
// Теоретичні питання:

// Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
// Цикли у програмуванні потрібні для виконання одного і тогож фрагменту коду певну  кількість
// раз без необхідності повторювати його. Цикли дозволяют втоматизувати процес обробки данних.

// Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.
// for використовував для написання лічильника, та коли потрібно було вирахувати числа кратні 5.
// for in використовували для того , щоб перебрати об'єкт.
// do while використовував коли потрібно було запитати у юзера значення і як що воно не відповідає 
// умові перепитувати користувача до того моменту , поки він введе коректну відповідь


// Що таке явне та неявне приведення (перетворення) типів даних у JS?
// Неявне перетворення, це автоматисчне перертворення типу данних залежно від умови за логікою JS
// Явне перетворення це коли розробник вказує як перетворити один тип данних в іньший

// Завдання
// Реалізувати програму на Javascript, яка знаходитиме всі числа кратні 5 (діляться на 5 без 
// залишку) у  заданому діапазоні. Завдання має бути виконане 
// на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Отримати за допомогою модального вікна браузера число, яке введе користувач.
// Вивести в консолі всі числа, кратні 5, від 0 до введеного користувачем числа. Якщо таких 
//чисел немає - вивести в консоль фразу "Sorry, no numbers"
// Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.
// Необов'язкове завдання підвищеної складності
// Перевірити, чи введене значення є цілим числом. Якщо ця умова не дотримується, повторювати
//  виведення вікна на екран, доки не буде введено ціле число.
// Отримати два числа, m і n. Вивести в консоль усі прості 
// числа (http://ru.math.wikia.com/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE)
//  в діапазоні від m до n (менше із введених чисел буде m, більше буде n). Якщо хоча б одне з 
//  чисел не відповідає умовам валідації, зазначеним вище, вивести повідомлення про
//   помилку і запитати обидва числа знову.

let enterNum 
 
do {
   enterNum = +prompt('Enter number') 
   if (!Number.isInteger(enterNum)) {
    alert('Invalid input, please enter a number')
   }
} while (!Number.isInteger(enterNum));
    

let multiple = enterNum
for (let i = 0; i <= multiple; i++) {
 if (i % 5 === 0) {
    console.log(i)
}
}
if (multiple < 5) {
    console.log('Sorry, no numbers')
}

